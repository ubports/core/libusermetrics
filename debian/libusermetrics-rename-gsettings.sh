#!/bin/sh

if [ "$(dconf list /com/lomiri/UserMetrics/ | wc -l)" -gt 0 ]; then
    printf 'Existing settings in /com/lomiri/UserMetrics/, skipping migration\n'
    exit 0
fi

# copy settings between namespaces, succeeds even if source is empty
dconf dump /com/canonical/UserMetrics/ | dconf load /com/lomiri/UserMetrics/
printf 'Migrated settings from /com/canonical/UserMetrics/ to /com/lomiri/UserMetrics/\n'
exit 0
